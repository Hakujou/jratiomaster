import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Token {
	public static void createToken(String announce, String hash) {
		File file = new File("./sessionStarted");
		try {
			file.createNewFile();
			BufferedWriter fw = new BufferedWriter( new FileWriter(file) );
			fw.write(announce);
			fw.newLine();
			fw.write(hash);
			fw.close();
		}
		catch (IOException e) {
			System.err.println("Erreur de création du token.");
		}
	}
	
	public static void deleteToken() {
		File file = new File("./sessionStarted");
		try {
			file.delete();
		}
		catch (Exception e) {
			System.err.println("Erreur de suppression du token.");
		}
	}
	
	public static boolean tokenExists() {
		File file = new File("./sessionStarted");
		if (file.exists())
			return true;
		return false;
	}
	
	public static String getTracker() {
		String tracker = null;
		try {
			FileReader file = new FileReader("./sessionStarted");
			BufferedReader br = new BufferedReader(file);
			tracker = br.readLine();
			br.close();
		}
		catch (FileNotFoundException e) {
			System.err.println("Erreur de récupération du token.");
		}
		catch (IOException e) {
			System.err.println("Erreur de lecture du fichier.");
		}
		return tracker;
	}
	public static String getHash() {
		String hash = null;
		try {
			FileReader file = new FileReader("./sessionStarted");
			BufferedReader br = new BufferedReader(file);
			br.readLine();
			hash = br.readLine();
			br.close();
		}
		catch (FileNotFoundException e) {
			System.err.println("Erreur de récupération du token.");
		}
		catch (IOException e) {
			System.err.println("Erreur de lecture du fichier.");
		}
		return hash;
	}
}
