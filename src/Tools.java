import jBittorrentAPI.Utils;

import java.util.Random;

public class Tools {
	public static void delay(int time) {
		try {
			Thread.sleep((int) time * 1000);
		} catch(InterruptedException ex) {
		    Thread.currentThread().interrupt();
		}
	}
	
	public static byte[] generateCliID(String manufID) {
        byte[] id = new byte[12];
        Random r = new Random(System.currentTimeMillis());
        r.nextBytes(id);
        return Utils.concat(manufID.getBytes(), id);
    }
	
	public static int randomSpeed(int baseSpeed, int randomCoeff) {
		int finalSpeed = (int) (baseSpeed + Math.random() * randomCoeff);
		return finalSpeed;
	}
}
