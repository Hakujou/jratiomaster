import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ClientEmu {
	public static byte[] loadPeerID(String name) {
		byte[] peerid = null;
		try {
			File file = new File("./ClientEmu/" + name);
			BufferedReader br = new BufferedReader( new FileReader(file) );
			String manufID = br.readLine();
			br.close();
			peerid = Tools.generateCliID(manufID);
		}
		catch (FileNotFoundException e) {
			System.err.println("Erreur d'ouverture du fichier emulation client.");
			System.exit(0);
		}
		catch (IOException e) {
			System.err.println("Erreur de lecture du fichier emulation client.");
			System.exit(0);
		}
		return peerid;
	}
	
	public static String loadUserAgent(String name) {
		String userAgent = null;
		try {
			File file = new File("./ClientEmu/" + name);
			BufferedReader br = new BufferedReader( new FileReader(file) );
			br.readLine();
			userAgent = br.readLine();
			br.close();
		}
		catch (FileNotFoundException e) {
			System.err.println("Erreur d'ouverture du fichier emulation client.");
			System.exit(0);
		}
		catch (IOException e) {
			System.err.println("Erreur de lecture du fichier emulation client.");
			System.exit(0);
		}
		return userAgent;
	}
}
