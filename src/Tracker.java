import jBittorrentAPI.BDecoder;
import jBittorrentAPI.TorrentFile;
import jBittorrentAPI.Utils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.Map;


public class Tracker {
	public static int updateInterval(Map respTracker, int updInterval, int defInterval, Boolean forceInterval) {
		int minInterval;
		
		if (respTracker != null) {
			if (respTracker.containsKey("interval") && respTracker.containsKey("min interval")) {
				minInterval = (int) (long) respTracker.get("min interval");
				if (updInterval == defInterval) {
					updInterval = (int) (long) respTracker.get("interval");
				}
				if (updInterval < minInterval && !forceInterval) {
					updInterval = minInterval;
				}
			}
		}
		
		return updInterval;
	}
	
	public static void printTrackerInfos(Map respTracker) {
		System.out.println("");
		if (respTracker != null)
			if (respTracker.containsKey("complete") && respTracker.containsKey("incomplete")) {
				System.out.println("--- Tracker infos ---");
				System.out.println("\tSeeders : " + (int) (long) respTracker.get("complete"));
				System.out.println("\tLeechers : " + (int) (long) respTracker.get("incomplete"));
				System.out.println("");
			} else {
				System.out.println(respTracker);
			}
	}
	
	public static void printRefreshInfos(int updInterval, int maxUpdLen, double execTime, long dataSent, int speed, int maxSpeedChar) {
		System.out.flush();
		
		String updTime = Integer.toString( (int) (updInterval - execTime) );
		while (updTime.length() < maxUpdLen) {
			updTime = "0" + updTime;
		}
		
		System.out.format("\rTracker update in " + updTime + "s");
		
		String dispValue = Double.toString( (Math.floor((double) (dataSent / 1024.0 / 1024.0) * 100)) / 100 );
		while (dispValue.length() - dispValue.indexOf('.') < 3)
			dispValue = dispValue + "0";
		
		System.out.format(" - Data amount : " + dispValue + "MB");
		
		String dispSpeed = Integer.toString(speed);
		
		while (dispSpeed.length() < maxSpeedChar)
			dispSpeed = "0" + dispSpeed;
		
		if (speed != 0)
			System.out.format(" (" + dispSpeed + "kBps)");
	}
	
	public static Map contactTracker(byte[] id, TorrentFile t, long dl, long ul, long left, String event, int inPort, String userAgent) {
		return contactTracker(id, t.announceURL, t.info_hash_as_url, dl, ul, left, event, inPort, userAgent);
	}
	
	public static Map contactTracker(byte[] id, String announceURL, String hash, long dl, long ul, long left, String event, int inPort, String userAgent) {
		try {
			URL source = new URL(announceURL + 
			  "?info_hash=" + hash + 
			  "&peer_id=" + Utils.byteArrayToURLString(id) + 
			  "&port="+ inPort +
			  "&downloaded=" + dl + "&uploaded=" + ul +
			  "&left=" + left + 
			  "&numwant=100&compact=1" +
			  event +
			  "&no_peer_id=1");
			
			System.out.println("Tracker request, URL : " + source);
			URLConnection uc = source.openConnection();
			System.setProperty("http.agent", "");
			uc.setRequestProperty("User-Agent", userAgent);
			InputStream is = uc.getInputStream();
			
			BufferedInputStream bis = new BufferedInputStream(is);

			Map m = BDecoder.decode(bis);
			bis.close();
			is.close();
			
			return m;
		} catch (MalformedURLException murle) {
			System.err.println("Bad Tracker URL");
			if (Token.tokenExists())
				Token.deleteToken();
			System.exit(0);
		} catch (UnknownHostException uhe) {
			System.err.println("Tracker non disponible... new try");
		} catch (IOException ioe) {
			System.err.println("Tracker unavailable... new try");
		} catch (Exception e) {
			System.err.println("Internal error");
			System.exit(0);
		}
		
		return null;
	}
}
