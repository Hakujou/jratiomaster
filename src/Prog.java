import java.util.Map;

import jBittorrentAPI.TorrentProcessor;
import jBittorrentAPI.TorrentFile;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class Prog {
	// Default update-interval for tracker
	public static final int defInterval = 600;
	
	// Retry delay on tracker refresh failure
	public static final int retryCrcDelay = 10;
	public static final int retryStdDelay = 30;
	
	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		Options options = new Options();
		
		// Required parameters
		options.addOption( OptionBuilder.withLongOpt("client")
										.withDescription("set client ID emulation profile")
										.hasArg()
										.isRequired()
										.withArgName("CLIENTEMU")
										.create("c") );
		options.addOption( OptionBuilder.withLongOpt("upload-speed")
										.withDescription("set upload speed rate")
										.hasArg()
										.isRequired()
										.withArgName("SPEED")
										.create("s") );
		options.addOption( OptionBuilder.withLongOpt("torrent-file")
				.withDescription("set torrent file used")
				.hasArg()
				.isRequired()
				.withArgName("FILE")
				.create("t") );
		
		// Additional parameters with required arguments
		options.addOption( OptionBuilder.withLongOpt("random-upload-speed")
				.withDescription("set random additional upload speed rate")
				.hasArg()
				.withArgName("SPEED")
				.create("rr") );
		options.addOption( OptionBuilder.withLongOpt("min-seeders")
				.withDescription("set minimum seeders amount required to update tracker data")
				.hasArg()
				.withArgName("SEEDERS")
				.create("ms") );
		options.addOption( OptionBuilder.withLongOpt("min-leechers")
				.withDescription("set minimum leechers amount required to update tracker data")
				.hasArg()
				.withArgName("LEECHERS")
				.create("ls") );
		options.addOption( OptionBuilder.withLongOpt("update-interval")
				.withDescription("set tracker update interval")
				.hasArg()
				.withArgName("INTERVAL")
				.create("u") );
		
		// Additionnal bool parameters
		options.addOption("h", "help", false, "prints the help content");
		options.addOption("fu", "force-update-interval", false, "force update interval to be set as update-interval parameter");
		
		// Command line parser and help formatter
		CommandLineParser parser = new GnuParser();
		HelpFormatter helper = new HelpFormatter();
		
		// Default vars for command line
		String torrentFile = null;
		String emuProfile = null;
		int seedSpeed = 0;
		int randSpeed = 0;
		int totalSpeed = 0;
		
		// Update-interval values
		int updInterval = defInterval;
		int minInterval = defInterval;
		Boolean forceInterval = false;
		int secInterval = 0;
		
		// Minimal seeders/leechers default constraints
		int minSeeders = 0;
		int minLeechers = 0;

		try {
			CommandLine line = parser.parse(options, args);
			
			torrentFile = line.getOptionValue("torrent-file");
			seedSpeed = Integer.parseInt( line.getOptionValue("upload-speed") );
			emuProfile = line.getOptionValue("client");
			
			if (line.hasOption("update-interval"))
				updInterval = Integer.parseInt( line.getOptionValue("update-interval") );
			
			if (line.hasOption("force-update-interval"))
				forceInterval = true;
			
			if (line.hasOption("min-seeders"))
				minSeeders = Integer.parseInt( line.getOptionValue("min-seeders") );
			
			if (line.hasOption("min-leechers"))
				minLeechers = Integer.parseInt( line.getOptionValue("min-leechers") );
			
			if (line.hasOption("random-upload-speed"))
				randSpeed = Integer.parseInt( line.getOptionValue("random-upload-speed") );
		}
		catch (ParseException e) {
			helper.printHelp("jratiomaster", options, true);
			System.exit(1);
		}
		
		Map respTracker = null;
		
		// Client emulation
		byte[] clientID = ClientEmu.loadPeerID(emuProfile);
		String userAgent = ClientEmu.loadUserAgent(emuProfile);
		
		// Parse torrent metadata file
		TorrentProcessor tp = new TorrentProcessor();
		TorrentFile torrent = tp.getTorrentFile( tp.parseTorrent(torrentFile) );
		
		// Undefined default seeders/leechers amount
		int seeders = -1;
		int leechers = -1;
		
		// Seed config
		long dataSent = 0; // Donn�es envoy�es (en Bytes)
		long oldDataSent = 0; // 
		int refreshRate = 2; // Intervalle de calcul des donn�es (en secondes)
		
		// If last exec of program was badly done, update tracker
		if (Token.tokenExists()) {
			System.out.println("\rClosing previous session...");
			while (Tracker.contactTracker(clientID, Token.getTracker(), Token.getHash(), 0, dataSent, 0, "&event=stopped", 13040, userAgent) == null) {
				Tools.delay(retryCrcDelay);
			};
			Token.deleteToken();
			System.out.println("Session closed.\r");
			System.out.println();
		}
		
		// Initial contact of tracker
		System.out.println("Starting new session...");
		do {
			respTracker = Tracker.contactTracker(clientID, torrent, 0, 0, 0, "&event=started", 13040, userAgent);
			Tools.delay(retryCrcDelay);
		} while (respTracker == null);
		
		// Get seeders/leechers
		if (respTracker.containsKey("incomplete") && respTracker.containsKey("complete")) {
			leechers = (int) (long) respTracker.get("incomplete");
			seeders = (int) (long) respTracker.get("complete");
		}
		
		// Refresh interval computing
		updInterval = Tracker.updateInterval(respTracker, updInterval, minInterval, forceInterval);
		secInterval = updInterval / 2;
		
		// Display config internal values
		int defUpdNbChar = Integer.toString(updInterval).length();
		int maxSpeedChar = Integer.toString(seedSpeed + randSpeed).length();
		
		// Display tracker infos
		Tracker.printTrackerInfos(respTracker);
		
		// Token creation
		Token.createToken(torrent.announceURL, torrent.info_hash_as_url);
		System.out.println("Tracker update done.");
		System.out.println("");
		
		// Added hook on program exit to update tracker and delete token
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				Token.deleteToken();
				System.out.println();
				System.out.println();
				System.out.println("Exiting...");
				while (Tracker.contactTracker(clientID, torrent, 0, 0, 0, "&event=stopped", 13040, userAgent) == null) {
					Tools.delay(retryCrcDelay);
				};
			}
		});
		
		// Timer init
		long initialTime = System.currentTimeMillis();
		double execTime = 0;
		int precValue = (int) Math.floor(execTime);
		
		while (true) {
			Tools.delay( Math.round(refreshRate / 2) );
			
			// If refresh rate is reached, update speed and data amount
			if ( ( ( (int) Math.round(execTime) % refreshRate ) == 0 ) )
				if ( (int) Math.round(execTime) != precValue) {
					// Security measures to avoid ban due to too high data upload
					if ((Math.round(execTime) - precValue) > secInterval) {
						System.out.println();
						System.out.println();
						System.err.println("Warning : Tracker was not polled since " + secInterval + "s");
						System.err.println("Tracker update will be shutted down to avoid ban due to bad speed");
						break;
					}
					totalSpeed = Tools.randomSpeed(seedSpeed, randSpeed);
					dataSent = totalSpeed * 1024 * refreshRate + dataSent;
					precValue = (int) Math.round(execTime);
				}
			
			// If tracker update interval is reached
			if (execTime >= updInterval) {
				System.out.println();
				System.out.println();
				System.out.println("Tracker update...");
				execTime = 0;
				initialTime = System.currentTimeMillis();
				
				// Update tracker
				do {
					if ((seeders == -1 && leechers == -1) || (seeders >= minSeeders && leechers >= minLeechers)) {
						respTracker = Tracker.contactTracker(clientID, torrent, 0, dataSent, 0, "", 13040, userAgent);
						oldDataSent = dataSent;
					} else {
						respTracker = Tracker.contactTracker(clientID, torrent, 0, oldDataSent, 0, "", 13040, userAgent);
					}
					Tools.delay(retryStdDelay);
				} while (respTracker == null);
				
				// If tracker update is done without issue
				if (respTracker != null) {
					// Refresh of seeders/leechers amount
					if (respTracker.containsKey("incomplete") && respTracker.containsKey("complete")) {
						leechers = (int) (long) respTracker.get("incomplete");
						seeders = (int) (long) respTracker.get("complete");
					}
					
					// Display tracker informations
					Tracker.printTrackerInfos(respTracker);
				}
			} else if (respTracker != null) {
				// Display informations
				if (Token.tokenExists())
					Tracker.printRefreshInfos(updInterval, defUpdNbChar, execTime, dataSent, totalSpeed, maxSpeedChar);
			}
			
			// Update timer
			execTime = (System.currentTimeMillis() - initialTime) / 1000.0;
		}
	}
}