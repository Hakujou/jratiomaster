#Jratiomaster.jar 
###Current build : 0.2.2

Jratiomaster is a project running on java. It allows you to cheat your ratio by sending false data to private trackers.

## Quick Start

The command for a quick start is:
```sh
java -jar jratiomaster.jar -c qbittorrent-3.1.12 -s 200 -t <yourfile.torrent>
```
It will start at 200kBps with qBittorrent 3.1.12 client emulation to the tracker defined by "yourfile.torrent". 
## Usage
### In order to call programm, use:
```sh
java -jar jratiomaster.jar
```
### ClientEmu
ClientEmu directory MUST be in the same directory than the executable. 

### Arguments:
 ```
 -c,--client <CLIENTEMU>            
 ```
 
 Set client ID emulation profile. Those names refers to file name in "ClientEmu" directory.
 The torrent clients you can now emulate are: 	
 
    * qbitorrent-3.1.11
    * qbitorrent-3.1.12
    * transmission-2.84
    * utorrent-2.2.1.0
	* rtorrent-0.9.4
	* vuze-5.5.0.0

 ```									 
 -s,--upload-speed <SPEED>           
 ```
 
 Set upload speed rate.
 The speed is kBps. Use an integer !
 
 ```
 -t,--torrent-file <FILE>           
 ```
 
  set torrent file that will be used.
 
### Optionnal Arguments:

 ```									 
 -h,--help                         
 ```
 
 Prints the help content
 
 ```
 -ls,--min-leechers <LEECHERS>      
 ```
 
 Set minimum leechers amount required to update tracker data.
 Default value is 0. If there's less leechers than the minimum leechers value, cheating will stop.
 
 ```									 
 -ms,--min-seeders <SEEDERS>         
 ```
 
 Set minimum seeders amount required to update tracker data.
 Default value is 0. If there's less seeders than the minimum seeders value, cheating will stop.
 
 
 ```									 
 -rr,--random-upload-speed <SPEED>  
 ```
 
 Set random additional upload speed rate.
 This option will make you have a random upload speed. Use an integer !
 The speed will be between (the speed specified with -s) and (the speed you set with -s + the value you used as argument for -rr)
 
 ```
 -u,--update-interval <INTERVAL>    
 ```
 
 Set tracker update interval in seconds. Use an integer !
 If the value is higher than maximum tracker's interval, you'll use that maximum as update interval.
 If the value is lower than minimum tracker's interval, you'll use that minimum as update interval.
 
 ```
 -fu,--force-update-interval         
 ```
 
 Force manual update interval to override tracker's min-interval even with lower value

## Contributors

- Romain Lecat
- Octave Monvoisin

## Dependencies

 * [jBittorrentAPI](http://sourceforge.net/projects/bitext/) 1.1
 * [Apache Commons CLI](http://commons.apache.org/proper/commons-cli/) 1.2
